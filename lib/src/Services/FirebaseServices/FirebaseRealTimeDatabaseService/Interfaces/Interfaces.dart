
abstract class BaseRealTimeDataBaseFirebase {
  //TODO: Please change this url for yours. ;)
  String baseUrl =
      "https://delivery-app-flutter-pruebas-default-rtdb.europe-west1.firebasedatabase.app/";
  //TODO: Please change this admin token for yours. ;)
  String adminToken = "AIzaSyDbWApYAtSOdT5n2TRvmWs6tcR5Q4Ed9IY";
  String endUrl = ".json";
}

abstract class RealtimeDataBaseService extends BaseRealTimeDataBaseFirebase {
  Future<Map<String, dynamic>> postData({ required Map<String, dynamic> bodyParameters, required String path });
  Future<Map<String, dynamic>> putData({ required Map<String, dynamic> bodyParameters, required String path });
  Future<Map<String, dynamic>> getData({ required String path });
}
