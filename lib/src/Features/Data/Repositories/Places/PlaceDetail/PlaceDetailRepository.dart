import 'package:delivery/src/Features/Data/Interfaces/Interfaces.dart';
import 'package:delivery/src/Features/Domain/Entities/Places/PlaceList/PlaceListEntity.dart';
import 'package:delivery/src/Services/FirebaseServices/FirebaseRealTimeDatabaseService/Interfaces/Interfaces.dart';
import 'package:delivery/src/Services/FirebaseServices/FirebaseRealTimeDatabaseService/Services/RealtimeDataBaseService.dart';

class DefaultPlaceDetailRepository extends PlaceDetailRepository {

  String _path = "placeList/";

  // * Dependencies
  final RealtimeDataBaseService _realtimeDataBaseService;

  DefaultPlaceDetailRepository({ RealtimeDataBaseService? realtimeDataBaseService })
      : _realtimeDataBaseService = realtimeDataBaseService ?? DefaultRealtimeDatabaseService();

  @override
  Future<void> savePlaceDetail({ required PlaceListDetailEntity placeDetail }) {
    var fullPath = _path + placeDetail.placeId;
    return _realtimeDataBaseService.putData(bodyParameters: placeDetail.toMap(), path: fullPath);
  }
}