import 'package:delivery/src/Features/Presentation/Shared/Components/Texts/TextView/View/TextView.dart';
import 'package:flutter/material.dart';
import '../../../../../../../Colors/colors.dart';

class PlaceDetailInfoView extends StatelessWidget {

  PlaceDetailInfoView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          margin: const EdgeInsets.symmetric(vertical: 7.0),
          child: const TextView(
              texto: "Boon Lay Ho Huat Fried Prawn Noodle",
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 30.0),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Row(
            children: const [
              Icon(Icons.location_on, color: greyColor),
              TextView(
                  texto: "03 Jameson Manors Apt. 177",
                  color: greyColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 15.0),
            ],
          ),
        )
      ],
    );
  }
}
